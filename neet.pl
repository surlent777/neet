#!/usr/bin/env perl

# FIXME
# When wanting to play the latest episode in an on-going show, the check for
# whether it's done happens before the adjustment check. This needs fixing.

# FIXME  Note also that when the show count is wrong and the question is
# answered "N", the results are strange. At 104 with 105 declared, + gave me
# episode 100 of Dragon Ball Super

use strict;
use warnings;
use 5.010;
use autodie qw(:all);

# Modules
use Carp;            # Core
use Config::Tiny;    # dpkg libconfig-tiny-perl || cpan Config::Tiny
use English qw(-no_match_vars);                              # Core
use File::Find;                                              # Core
use File::Path qw(make_path);                                # Core
use Getopt::Long qw(:config no_ignore_case pass_through);    # Core
use Pod::Usage;                                              # Core
use Sort::Naturally;    # dpkg libsort-naturally-perl || cpan Sort::Naturally

## no critic (RequirePodSections) # not all sections needed
## no critic (RequireLocalizedPunctuationVars)
BEGIN {
    $ENV{Smart_Comments} = " @ARGV " =~ /--debug|-d/xms;
}
use Smart::Comments -ENV
    ;                   # dpkg libsmart-comments-perl || cpan Smart::Comments

our $VERSION = '0.8';

## CONFIGURATION

# Initialise config variables; will be set in check_config()
my ( $config_dir, $config_file, $list_file, $alias_file, );
my ( $player, $player_options, $media_dir, );

my ( $FILTERS, $VALID_EXTENSIONS );

# Initialise main variables
my ( $active_show, $binge, $choose_show, $current_episode, $total_episodes,
    $episode_path, );
$binge = -1;

## COMMAND-LINE ARGUMENTS

my ( $increase, $decrease );
### @ARGV
foreach (@ARGV) {
    if (/^[+]+$/xms) {
        my @plus_count = split //xms;
        ### @plus_count
        $increase += $#plus_count + 1;
        ### $increase
    }

    if (/^[-]+$/xms) {
        my @minus_count = split //xms;
        ### @minus_count
        $decrease += $#minus_count + 1;
        ### $decrease
    }
}

GetOptions(

    'active-show|a:-1'    => \&show_handler,
    'add-show=s'          => \&show_handler,
    'binge|b=i'           => \$binge,
    'choose-episdode|c=i' => \my $chosen_episode,
    'edit|e'              => sub {
        check_config();
        if ( $OSNAME ne 'MSWin32' ) {
            exec "$ENV{EDITOR}", "$list_file", "$alias_file", "$config_file",
                or croak "$ERRNO";
        }
        else {
            for ( $list_file, $alias_file, $config_file ) {
                system "start notepad.exe $_",
                    and croak "$ERRNO";
            }
            exit;
        }
    },
    'list|l'        => \&print_list,
    'remove-show=s' => \&show_handler,
    'help|h'        => \my $help,
    'debug|d'       => \my $debug,       # dummy variable
    'man'           => \my $man,
    'version|v'     => \my $version,

) or pod2usage( -verbose => 0 );

if ($help) {
    pod2usage( -verbose => 0 );
}
if ($man) {
    pod2usage( -verbose => 2 );
}
if ($version) {
    die "$PROGRAM_NAME v$VERSION\n";
}

## FUNCTIONS

## no critic [ProhibitExcessComplexity]
# This function ensures needed directories and files exist
sub check_config {
    if ( $OSNAME ne 'MSWin32' ) {
        $config_dir = "$ENV{'HOME'}/.config/neet";
    }
    else {
        $config_dir = "$ENV{'APPDATA'}/neet";
    }
    if ( !-d $config_dir ) {
        ### Creating config dir $config_dir
        make_path $config_dir or croak "$ERRNO";
    }

    $config_file = "$config_dir/neet.conf";
    if ( !-e $config_file ) {
        ### Creating config file $config_file

        # Set player and base media directory...
        if ( $OSNAME ne 'MSWin32' ) {
            $player    = 'smplayer';
            $media_dir = "$ENV{'HOME'}/Videos/Anime";
        }
        else {
            $player    = 'C:\Program Files\SMPlayer\smplayer.exe';
            $media_dir = "$ENV{'HOMEDRIVE'}$ENV{'HOMEPATH'}/Videos/Anime";
        }
        $player_options   = '-actions "fullscreen"';
        $VALID_EXTENSIONS = '.avi, .flv, .mkv, .mp4, .ogm, .rm';
        $FILTERS = 'Bonus, Extras, Films, OP and ED, Special Features';

        # ...then write a default conf file
        my $config = Config::Tiny->new;
        $config->{_}->{player}           = $player;
        $config->{_}->{player_options}   = $player_options;
        $config->{_}->{media_dir}        = $media_dir;
        $config->{_}->{valid_extensions} = $VALID_EXTENSIONS;
        $config->{_}->{filters}          = $FILTERS;
        $config->write("$config_file");
        printf
            "Config file written at %s; launching EDITOR for manual verification [Press Enter to continue]\n",
            $config_file;
        my $wait = readline *STDIN;

        if ( $OSNAME ne 'MSWin32' ) {
            system "$ENV{'EDITOR'}", "$config_file", and croak "$ERRNO";
        }
        else {
            system "start notepad.exe $config_file" and croak "$ERRNO";
        }
    }
    else {
        # Open the config file...
        my $config = Config::Tiny->new;
        $config = Config::Tiny->read("$config_file");

        # ...then set player and base media directory
        $player           = $config->{_}->{player};
        $player_options   = $config->{_}->{player_options};
        $media_dir        = $config->{_}->{media_dir};
        $VALID_EXTENSIONS = $config->{_}->{valid_extensions};
        $FILTERS          = $config->{_}->{filters};
    }

    # Lastly, set up the regexes
    my @extensions = split /,\s/xms, $VALID_EXTENSIONS;
    my @patterns = map {qr/\Q$_\E$/ixms} @extensions;
    $VALID_EXTENSIONS = join q(|), @patterns;

    my @filters = split /,\s/xms, $FILTERS;
    @patterns = map {qr/\Q$_\E/ixms} @filters;
    $FILTERS = join q(|), @patterns;

    ### $player
    ### $player_options
    ### $media_dir
    ### $VALID_EXTENSIONS
    ### $FILTERS
    $alias_file = "$config_dir/alias";
    if ( !-e $alias_file ) {
        ### Creating alias file $alias_file
        open my $fh, '>', "$alias_file" or croak "$ERRNO";
        close $fh or croak "$ERRNO";
    }
    $list_file = "$config_dir/list";
    if ( !-e "$list_file" || -z "$list_file" ) {
        printf
            "Missing the list of TV shows. Add a show with --add-show or --edit.\nA template is being created for you in $list_file.\n";

        my $explanation = <<'EOF';
# These comments, and any others, will vanish the next time this file is
# modified by the script.
#
# List your show's name on the first line and then on the next line list the
# current/total episodes. Add a + in front of the episode for the current show.
# Its folder needs to be under your media folder and named similarly.
#
# A simple example:
#
# Madoka
# + 1/12
# Evangelion
# 1/26
#
# Should you prefer to have this done automatically, run this script with
# --add-show "Show name"
EOF
        open my $fh, '>', "$list_file" or croak "$ERRNO";
        print {$fh} $explanation or croak "$ERRNO";
        close $fh or croak "$ERRNO";
    }
    return;
}

# This function gets basic media info, i.e. the active show name, episode count and total episodes
sub get_info {

    # Outputs entire list file
    my @list = get_list();

    # Get the active show (denoted with a '+')
    for my $i ( 0 .. $#list ) {
        if ( $list[$i] =~ /[+]/xms ) {
            $active_show = $list[ $i - 1 ];

            # Get the show name, current episode and total episode count
            ($current_episode) = ( $list[$i] =~ /([[:digit:]].*)\//xms );
            ($total_episodes)
                = ( $list[$i] =~ /[[:digit:]].*\/([[:digit:]].*)/xms );
            ### $active_show
            ### $total_episodes

            # Increase or decrease the episode count if needed
            if ( $increase || $decrease || $chosen_episode ) {
                adjust_count( @list, $i );
            }

            elsif ( $binge != -1 ) {
                binge_helper( @list, $i );
            }
            last;
        }
    }
    ### $current_episode

    # If no active show, fix that
    if ( !$active_show ) {
        choose_show(@list);
        get_info();
    }
    return;
}

# This function sorts the list file, which is slightly more complicated than just calling sort()
sub sort_list {
    my @list = @_;
    my @buffer;
    for my $i ( 0 .. $#list ) {
        if ( $i % 2 == 0 ) {
            my $rec = join "\0", $list[$i], $list[ $i + 1 ];
            push @buffer, $rec;
        }
    }
    @list   = ();
    @buffer = sort @buffer;
    ### @buffer
    foreach (@buffer) {

        if (/(.*)\0(.*)/xms) {
            push @list, $1, $2;
        }
    }
    return @list;
}

# This function prints the current shows' ID, alias, path, and episode count
sub print_list {
    check_config();

    # Get list contents and fail if no shows are defined
    my @list = get_list();
    if ( $#list <= 0 ) {
        printf {*STDERR}
            "No shows are currently defined. Add a show with --add-show or --edit.\n";
        exit 1;
    }

    get_info();

    # Get alias contents; extract the paths
    my @alias = get_alias();
    foreach (@alias) {
        s/^.*=(.*)/$1/xms;
    }

    # Separate the elements of the list file
    my ( @shows_trimmed, @episodes_trimmed );

    for my $i ( 0 .. $#list ) {
        if ( $list[$i]
            !~ /[[:blank:]]*[+]?[[:blank:]]*[[:digit:]]+\/[[:digit:]]+/xms )
        {
            push @shows_trimmed, $list[$i];
        }
        else {
            if ( $list[$i] =~ /^[+][[:blank:]]/xms ) {
                $list[$i] =~ s/[+][[:blank:]]//xms;
            }
            push @episodes_trimmed, $list[$i];
        }
    }
    chomp @shows_trimmed;
    chomp @episodes_trimmed;

    # Sanity check
    if ( $#alias != $#shows_trimmed ) {
        printf {*STDERR} "The number of entries in %s and %s do not match\n",
            $alias_file, $list_file;
        exit 1;
    }

    # Pretty-print
    printf "Active show is %s\n\n", $active_show;

    for my $i ( 0 .. $#shows_trimmed ) {
        $i++;
        print
            "[$i]: [$shows_trimmed[$i-1]] [$alias[$i-1]] [$episodes_trimmed[$i-1]]\n";
    }
    exit;
}

# This function saves the list file
sub write_list_file {
    my @list = @_;

    open my $fh, '>', "$list_file" or croak "$ERRNO";
    foreach (@list) {
        print {$fh} "$_\n";
    }
    close $fh or croak "$ERRNO";
    return;
}

# This function adjusts the count of the active show
sub adjust_count {

    my @list = @_;
    my $i    = pop @list;
    @list = sort_list(@list);
    ### $i
    if ($chosen_episode) {
        if ( $chosen_episode > $total_episodes || $chosen_episode <= 0 ) {
            printf {*STDERR}
                "The chosen episode (%d) is outside the range of given episodes (%d)\n",
                $chosen_episode, $total_episodes;
            exit 1;
        }
        $current_episode = $chosen_episode;
    }
    elsif ($increase) {
        $current_episode += $increase;
    }
    else {
        $current_episode -= $decrease;
    }

    if ( $current_episode <= $total_episodes ) {

        if ( $current_episode <= 0 ) {
            printf "WARNING: Current episode was <= 0. Setting to 1\n";
            $current_episode = 1;
        }

        if ( $binge != -1 ) {
            binge_helper( @list, $i );
            ### $binge
            return;
        }
        else {
            $list[$i] =~ s/.*/\+ $current_episode\/$total_episodes/xms;
        }

        write_list_file(@list);
        if ( !$chosen_episode ) {
            if ($increase) {
                printf "Increasing episode count\n";
            }
            else {
                printf "Decreasing episode count\n";
            }
        }

    }

    elsif ( $current_episode > $total_episodes ) {
        my $response = q{};
        while ( $response !~ /^y(?:es)?$|^no?$/ixms ) {
            printf
                "You've passed the final episode. Remove the show from the list? [Y/N]\n";
            $response = readline *STDIN;
            if ( $response =~ /^y(?:es)?$/ixms ) {
                $i--;
                @list = remove_show( @list, $i );
                choose_show(@list);
                exit;
            }
            elsif ( $response =~ /^no?$/ixms ) {

                $list[$i] =~ s/.*/\+ 1\/$total_episodes/xms;
                write_list_file(@list);
                my $response2 = q{};
                while ( $response2 !~ /^y(?:es)?$|^no?$/ixms ) {
                    printf
                        "Setting current episode to 1. Choose a new default show? [Y/N]\n";
                    $response2 = readline *STDIN;
                    if ( $response2 =~ /^y(es)?$/ixms ) {
                        choose_show(@list);
                        exit;
                    }
                    elsif ( $response2 =~ /^no?$/ixms ) {
                        die "Exiting\n";
                    }
                }
            }
        }
    }
    return;
}

# This function handles --add-show, --remove-show, and --active-show
sub show_handler {
    my ( $arg, $show ) = @_;

    # Remove potential trailing '/' from $show in case tab-completion was used
    $show =~ s/\/$//xms;

    ### $arg
    ### $show
    check_config();

    # Get list contents and fail if no shows are defined
    my @list = get_list();
    if ( $#list <= 0 && $arg !~ /add/ixms ) {
        printf {*STDERR}
            "No shows are currently defined. Add a show with --add-show or --edit\n";
        exit 1;
    }

    if ( $arg =~ /active/ixms ) {

        get_info();
        $choose_show = $show;
        ### $choose_show
        choose_show(@list);
        exit;
    }

    elsif ( $arg =~ /remove/ixms ) {

        get_info();

        # If given a number, remove the corresponding show
        if ( $show =~ /^[[:digit:]]+$/xms ) {
            if ( $show > @list / 2 || $show <= 0 ) {
                printf {*STDERR} "No matching shows found. Exiting.\n";
                exit 1;
            }

           # The show number to array position is an increasing series: show 1
           # is in position 0, show 2 is in position 1, show 3 is in position
           # 4, etc. To get the right number, we calculate the difference
           # between the show number and 2, then add it
            my $i;
            my $diff = $show - 2;
            $i = $show + $diff;

           # Backup the real active show so it doesn't display false output on
           # choose_show()
            my $active_show_bak = $active_show;
            $active_show = $list[$i];
            ### $active_show
            @list = remove_show( @list, $i );
            $active_show = $active_show_bak;
            choose_show(@list);
            exit;
        }

        # If given a name instead, find the show and remove it
        for my $i ( 0 .. $#list ) {
            if ( $list[$i] eq $show ) {

           # Backup the real active show so it doesn't display false output on
           # choose_show()
                my $active_show_bak = $active_show;
                $active_show = $list[$i];
                ### $active_show
                @list = remove_show( @list, $i );
                $active_show = $active_show_bak;
                ### $active_show
                choose_show(@list);
                exit;
            }
            elsif ( $i == $#list ) {
                printf {*STDERR} "No matching shows found. Exiting.\n";
                exit 1;
            }
        }
    }
    elsif ( $arg =~ /add/ixms ) {

        # Check for a duplicate show name
        foreach (@list) {
            if (/^$show$/ixms) {
                print "$show is already present\n";
                exit;
            }
        }
        $active_show = $show;
        add_show( @list, $show );
        exit;
    }
    return;
}

# This function removes an expired show
sub remove_show {
    my @list = @_;
    my $i    = pop @list;

    # Remove the entries and re-write the list file
    splice @list, $i, 2;
    write_list_file(@list);

    my @alias = get_alias();

    # The separate filehandle name keeps perlcritic happy. Apparently a bug.
    open my $fh2, '>', "$alias_file" or croak "$ERRNO";
    foreach (@alias) {

        # Write everything except the active show, which is now over
        if ( !/\Q$active_show\E=.*?\//ixms ) {
            print {$fh2} "$_\n";
        }
    }
    close $fh2 or croak "$ERRNO";
    return @list;
}

# This function sets the active show interactively
sub choose_show {

    # Cut out the numbers and get just the shows
    # Assumes the input file is in the correct format
    my @list = @_;

    if ( $#list <= 0 ) {
        print "No other shows to choose from. Exiting\n";
        exit;
    }

    my @shows_trimmed;
    my @episodes_trimmed;
    my $previous_show;
    if ( !$active_show ) {
        $previous_show = 'not declared';
    }
    else {
        $previous_show = $active_show;
    }
    for my $i ( 0 .. $#list ) {

        # Filter out the episode counts
        if ( $list[$i]
            !~ /[[:blank:]]*[+]?[[:blank:]]*[[:digit:]]+\/[[:digit:]]+/xms )
        {
            push @shows_trimmed, $list[$i];
        }
        else {
            if ( $list[$i] =~ /^[+][[:blank:]]/xms ) {
                $list[$i] =~ s/[+][[:blank:]]//xms;
            }
            push @episodes_trimmed, $list[$i];
        }
    }

    chomp @shows_trimmed;
    chomp @episodes_trimmed;
    ### @shows_trimmed

    my $number_of_choices = $#shows_trimmed + 1;
    my $selection;
    if ( !$choose_show || $choose_show == -1 ) {
        printf "Previous active show was %s\n",    $previous_show;
        printf "Select the desired show [1-%d]\n", $number_of_choices;
        $selection = -1;
        while ( $selection < 0 || $selection > $#shows_trimmed ) {
            for my $i ( 0 .. $#shows_trimmed ) {
                $i++;
                print
                    "[$i]: $shows_trimmed[$i-1] ($episodes_trimmed[$i-1])\n";
            }
            $selection = readline *STDIN;
            $selection--;
        }
    }
    else {
        $selection = $choose_show - 1;
    }
    $active_show = $shows_trimmed[$selection];

    for my $i ( 0 .. $#list ) {
        if ( $list[$i] =~ /\Q$previous_show\E$/ixms ) {

            # Remove + from line below
            $list[ $i + 1 ] =~ s/[[:blank:]]*[+][[:blank:]]*//ixms;
        }
        if ( $list[$i] =~ /\Q$active_show\E$/ixms ) {

            # Add + to line below
            $list[ $i + 1 ] =~ s/(.*)/\+ $1/ixms;
        }
    }
    chomp @list;

    write_list_file(@list);
    printf "Selected %s\n", $active_show;
    return;
}

# This function adds a show
sub add_show {
    my @list = @_;
    my $show = pop @list;
    if ( !check_alias() ) {
        my $directory = get_directory();
        make_alias($directory);
    }
    my @episodes       = get_episodes();
    my $episode_count  = $#episodes + 1;
    my $episode_string = "1/$episode_count";
    ### $episode_string
    push @list, $show, $episode_string;
    ### @list
    @list = sort_list(@list);
    write_list_file(@list);

    printf "%s added (%d episodes)\n", $show, $episode_count;
    if ( $#list == 1 ) {

        # Auto-activate since it's the first show
        if ( $list[0] =~ /\Q$active_show\E$/ixms ) {
            $list[1] =~ s/(.*)/\+ $1/ixms;
            write_list_file(@list);
        }
    }
    return;
}

# This function is a helper for --binge
sub binge_helper {

    ### $binge
    my @list = @_;
    my $i    = pop @list;

    if ( $binge - 1 + $current_episode <= $total_episodes ) {
        my $adjusted_current_episode = $binge - 1 + $current_episode;
        $list[$i] =~ s/.*/\+ $adjusted_current_episode\/$total_episodes/xms;
    }
    elsif ( $binge != -1 && $binge + $current_episode > $total_episodes ) {
        printf
            "WARNING: Binging attempted to go past the total number of episodes. Correcting.\n";

        # Get the difference between where we are and the end of the line
        $binge = $total_episodes - $current_episode + 1;
        $list[$i] =~ s/.*/\+ $total_episodes\/$total_episodes/xms;
    }
    write_list_file(@list);
    return;
}

# This function checks whether alias is available for the active show
sub check_alias {

    # Check if the show already has an alias...
    my @alias = get_alias();
    ### @alias
    foreach (@alias) {

        # ...If so, set the episode path...
        if (/\Q$active_show\E=(.*\/)/ixms) {
            $episode_path = $1;
            ### $episode_path
            return 1;
        }
    }

    # ...else return normally
    return;
}

# This function creates an alias for the active show
sub make_alias {

    # Create alias
    my $directory = shift;
    my @alias     = get_alias();

    # Check for a duplicate directory
    foreach (@alias) {
        if (/$directory\/$/ixms) {
            print "$active_show is already present\n";
            exit;
        }
    }

    push @alias, "$active_show=$directory/";
    @alias = sort @alias;
    open my $fh, '>', "$alias_file" or croak "$ERRNO";
    foreach (@alias) {
        print {$fh} "$_\n";
    }
    close $fh or croak "$ERRNO";
    $episode_path = "$directory/";
    ### $episode_path
    printf "Show initialised. (alias set)\n";
    return;
}

# This function opens and sorts the alias file
sub get_alias {

    open my $fh, '<', "$alias_file" or croak "$ERRNO";
    my @alias = <$fh>;
    close $fh or croak "$ERRNO";
    chomp @alias;

    # Remove comments and blanks, then sort
    @alias = grep { !/(?:^\#)|(?:^[[:blank:]]+$)|(?:^$)/xms } @alias;
    @alias = sort @alias;
    return @alias;
}

# This function opens and sorts the list file
sub get_list {
    open my $fh, '<', "$list_file" or croak "$ERRNO";
    my @list = <$fh>;
    close $fh or croak "$ERRNO";
    chomp @list;

    # Remove comments and blanks, then sort
    @list = grep { !/(?:^\#)|(?:^[[:blank:]]+$)|(?:^$)/xms } @list;
    @list = sort_list(@list);
    ### @list
    return @list;
}

# This function tries to fuzzy find the right directory
sub get_directory {

    # Recurses through all directories in $media_dir
    my @matches;
    find(
        sub {
            # If a file is not a directory, exclude it from the listing
            if ( !-d ) {
                return;
            }

# If a directory isn't related to the current show, exclude it from the listing
            elsif ( /\Q$active_show\E/ixms
                || $File::Find::dir =~ /\Q$active_show\E/ixms )
            {
                # If there's no videos, exclude it from the listing
                opendir my $DIR, $_ or croak "$ERRNO";
                my $vids = grep {/$VALID_EXTENSIONS/ixms} readdir $DIR;
                closedir $DIR or croak "$ERRNO";
                ### $vids
                if ($vids) {
                    push @matches, $File::Find::name;
                }
            }
        },
        $media_dir
    );

    if ( !@matches ) {
        printf {*STDERR} "No valid directories found in %s\n", $media_dir;
        exit;
    }

    my @matches_clone;
    foreach (@matches) {

        # Get rid of the base path and flatten the rest
        push @matches_clone, split /\Q$media_dir\E\//xms;
    }
    @matches = @matches_clone;
    @matches = grep { !/(?:^[[:blank:]]+$)|(?:^$)|(?:$FILTERS)/ixms }
        @matches
        ; # Gets rid of garbage that shows up after the split and applies user-defined filters

    @matches = sort @matches;
    ### @matches
    if ( @matches == 1 ) {
        return $matches[0];
    }
    elsif ( @matches > 1 ) {
        my $selection = -1;
        while ( $selection < 0 || $selection > $#matches ) {
            my $number_of_choices = $#matches + 1;
            printf "There are %s directories that contain %s\n",
                $number_of_choices, $active_show;
            printf "Which directory would you like to use? [1-%s]\n",
                $number_of_choices;
            for my $i ( 0 .. $#matches ) {
                $i++;
                print "[$i]: $matches[$i-1]\n";
            }
            $selection = readline *STDIN;
            $selection--;
        }
        return $matches[$selection];
    }
    else {
        die "No directory match found; use 'neet -d' for debug info\n";
    }
}

# This function gets a list of eligible episodes
sub get_episodes {

    # Lists all video files in the directory
    my @potential_episodes;
    my @episodes;
    opendir my $DIR, "$media_dir/$episode_path" or croak "$ERRNO";
    @potential_episodes = grep {/$VALID_EXTENSIONS/ixms} readdir $DIR;
    closedir $DIR or croak "$ERRNO";

    # Anything that's an episode needs to have a number
    @potential_episodes = grep {/[[:digit:]]+/ixms} @potential_episodes;

    # Clean up and naturally sort, then remove extraneous files
    chomp @potential_episodes;

    ### @potential_episodes

    # Create a hash, then check for subtitler prefixes, e.g. [Commie]
    # as well as suffixes, e.g. [720x480 h264]
    my %episodes;
    foreach (@potential_episodes) {
        if ( !/\[.*]/gxms ) {
            $episodes{$_} = $_;
        }

        else {
            # Remove prefixes and suffixes
            ( my $clean = $_ ) =~ s/\[.*?](?:[[:blank:]]|_|)//gxms;

          # Clear out everything up to the first digit and associate it with
          # the episode. Checks for formats similar to "Episode 12v2" as well.
            if ( $clean =~ /\b([[:digit:]]{1,3})(?:v[[:digit:]]{1})?\b/ixms )
            {
                $clean = $1;
                $episodes{$_} = $clean;
            }

            # Similar to the above, but if \b fails, try \w
            elsif ( $clean =~ /\w([[:digit:]]{1,3})\w/ixms ) {
                $clean = $1;
                $episodes{$_} = $clean;
            }

        }
    }
    ### %episodes
    # Die if in the end we found nothing
    if ( !%episodes ) {
        croak "No valid episodes found\n";
    }

# Otherwise, sort the hash keys by value so that the show file names end up sorted
    @potential_episodes
        = sort { ncmp( $episodes{$a}, $episodes{$b} ) } keys %episodes;

    ### @potential_episodes

    # Check whether the totals add up

 # If being called from add_show(), set $total_episodes so the episode balance
 # checker doesn't get triggered
    if ( defined( ( caller 1 )[3] ) && ( caller 1 )[3] eq 'main::add_show' ) {
        $total_episodes = scalar @potential_episodes;
    }

    if ( @potential_episodes != $total_episodes ) {

        # TODO: Do this check earlier. As early as physically possible.
        printf
            "WARNING: The total number of episodes (%d) doesn't match what has been detected (%d).\n",
            $total_episodes, $#potential_episodes + 1;
        my $response = q();
        while ( $response !~ /^y(?:es)?$|^no?$/ixms ) {
            print "Adjust the episode count? [Y/N]\n";
            $response = readline *STDIN;
            if ( $response =~ /^y(?:es)?$/ixms ) {
                $total_episodes = $#potential_episodes + 1;
                my @list = get_list();
                for my $i ( 0 .. $#list ) {
                    if ( $list[$i] =~ /[+]/xms ) {
                        $list[$i] =~ s{\/\d+}{\/$total_episodes}xms;
                        write_list_file(@list);
                    }
                }
            }
            elsif ( $response =~ /^no?$/ixms ) { }
        }
    }

    return @potential_episodes;
}

# This function tries to get the right file name
sub get_current_episode_name {

    my @episodes = @_;
    my @episode_names;
    my $path = $media_dir . q{/} . $episode_path;

    for ( -1 .. $binge ) {
        push @episode_names,

# Quotes here are added so that giving the video player the raw array actually works
            q{"} . $path . $episodes[ $current_episode + $_ ] . q{"};

        # Apparently this isn't allowed in the 'for' declaration itself:
        last if ( $_ == $binge - 2 );
    }
    ### @episode_names
    if ( !@episode_names ) {
        die
            "Episode $current_episode not found in $media_dir/$episode_path\n";
    }
    @episode_names = join q{ }, @episode_names;
    return @episode_names;
}

# This function starts the video player
sub play {
    my @current_episodes = @_;
    if ( $binge == -1 ) {
        printf "Playing %s episode %d/%d\n", $active_show,
            $current_episode,
            $total_episodes;
    }
    else {
        printf "Playing %s episodes %d-%d/%d\n", $active_show,
            $current_episode, $current_episode + $binge - 1,
            $total_episodes;
    }
    if ( $OSNAME ne 'MSWin32' ) {
        exec "$player $player_options @current_episodes 1> /dev/null"
            or croak "$ERRNO";
    }
    else {
        exec "$player", "$player_options", "@current_episodes"
            or croak "$ERRNO";

    }
}

## EXECUTION
check_config();
get_info();
if ( !check_alias() ) {
    my $directory = get_directory();
    make_alias($directory);
}
my @episodes         = get_episodes();
my @current_episodes = get_current_episode_name(@episodes);

play(@current_episodes);

__END__

=pod Changelog

=begin comment

Changelog:

0.8
-Added explicit Windows support

0.7c
-Added a check to allow adjusting the total number of episodes if a discrepency is found.

0.7b1
-Consolidated the new regex with the first

0.7b
-Adjusted episode-finding algorithm to attempt to account for "v2" episodes, e.g. "Nichijou - 12v2 [720p].mkv"
-Added autodie to silence mistaken perlcritic lines

0.7a
-Corrected improper messages on decreasing episode count or when -c is used in
conjunction with + or -

0.7
-Ensured that fatal errors get redirected to STDERR
-Added --choose-episode functionality in further mimicry of the original script
-Adjusted the usage information to be more in-line with tradition
-Simplified the episode detection algorithm somewhat, fixing a dumb bug in the
process

0.6a
-Adjusted --remove-show to correct an oversight which made it impossible to remove shows with more than one word in the title
-Corrected a few minor stylstic code oversights
-Made minor corrections to the manual

0.6
-Adjusted --binge code in binge_helper() to correct behaviour when binging
beyond the actual number of episodes instead of just calling die()
-Adjusted behaviour when trying to watch an episode < 0, simply setting it to 1
-Refactored increase_count() to make use of binge_helper() instead of
duplicating code
-Refactored decrease_count() to more accurately mirror the
refactored increase_count(), but then realised they were identical and
consolidated them into adjust_count()
-Refactored to use a sub called write_list_file() as appropriate

0.5
-Added --binge argument and code
-Changed episode detection again, defaulting to \b for matching with \w as a
fallback if nothing is matched. We'll get those bastards yet.
-Added error-checking code to the episode detection so that it dies if no valid
episodes are found by the regexes
-Changed get_current_episode() to get_current_episode_name() to avoid ambiguity
-Updated documentation to reflect the new functionality

0.4c
-Replaced \b with \w in get_episodes()'s main regex to cover underscores

0.4b
-Re-arranged get_info() placement in the show_handler() sub to avoid a condition
wherein adding shows was impossible
-Fixed inability to remove a show by number with only one show remaining
-Adjusted get_episodes() to strip subtitler tags (if present) when ordering the
episodes via a hash
-Adjusted get_episodes()'s regexes to cover more cases reliably; while doing so
also found that it was possible to greatly simplify the sub-procedure and
remove the need for Text::Levenshtein
-Removed in-line TODO list

0.4a
-Improved get_directory() performance on larger filesystems
-Added a check for identical shows when adding new shows via --add-show

0.4
-Made $episode_path's regex a little more greedy to allow for depth/recursion
-Improved get_directory() to allow for nested directories and multiple seasons
-Fixed mistaken implementation of --remove-show which caused unexpected
behaviour when removing a show by number
-Corrected --remove-show operations further by ensuring the correct previous
active show was shown
-Adjusted $POTENTIAL_EPISODES regex to include dashes
-Checks for an empty list file and exits where appropriate, e.g. after removing the final/only show
-Now auto-activates the first show added
-Made an add_show() sub which is called from show_handler() similarly to remove_show()
-Added $FILTERS and $VALID_EXTENSIONS to the config file
-The array of matching shows when adding an episode is now sorted for aesthetic pleasure
-Updated man page as appropriate

0.3
-RealMedia files are now recognised in the episode selection regex
-The exec() call for launching the editor is now in list form
-Most print commands have been replaced with printf
-[+] and [-] arguments are now counted regardless of spaces
-Configuration file now gets launched in $EDITOR after creation for manual
verification
-If there was no previous show detected on -a, the output now makes sense and
avoids the numerous warning messages
-Explicity defined utf8 encoding for the main program and the man page
-Moved the increment/decrement checks out of check_info() and into
show_handler()
-Added dedicated subs for opening the alias and list files
-When reading the alias and list files blank lines, empty lines, and commented
lines are ignored
-Added a one-off commentary/template to the list file on creation
-get_directory() now does the entirety of the fuzzy logic selection instead of
delegating part of that to make_alias(), returning a single match. Adjusted
main logic accordingly
-Made the first-run process less painful (hopefully)
-Fixed bug that arose when a show wasn't set as active in the initial
writing of the list file on the first run
-Split up get_episode() into get_episodes() and get_current_episode()
-Changed the way the current episode is found; now uses Levenshtein distance
-Implemented --add-show
-Updated the man page

0.2
-Added ini file via Config::Tiny for setting constants (player, player options,
base directory)
-Various code cleanups
-Now shows episode stats with --active-show
-Finished shows now reset to episode 1 if not removed
-Added --list to neatly print current shows' IDs, aliases, and episode counts
-Ensured alias and list files are sorted on each read and before writing
-Added IDs to shows, visible with --list and -a; the ID can be used for switching
shows with -a and --remove-show
-Added --remove-show and prototyped --add-show

0.1
-Initial version; roughly feature-for-feature clone of neet.sh with additional
sanity checks, support for multiple increment/decrements, and minor
discoverability-related enhancements.

=end comment

=cut

# Documentation
=pod

=encoding utf8

=head1 NAME

Neet - Utility to help manage animu watching

=head1 USAGE

 neet.pl [OPTION]...

 +                          Increments active show count and plays the episode
                              Multiple + signs may be specified
 -                          Decrements active show count and plays the episode
                              Multiple - signs may be specified
 -a, --active-show [id]     Selects active show either interactively or by ID
 -b, --binge <num>          Queues up <num> shows starting from the current episode;
                              affected by + and -
 -c, --choose-episode <num> Chooses the given episode. Overrides + and -.
 -e, --edit                 Launches $EDITOR for manual editing of the list/alias files
 -l, --list                 Prints current shows' ID, alias, path, and episode count
     --add-show             Adds a show by name
     --remove-show [id]     Removes a show by name or ID
 -h, --help                 Display this help text
 -d, --debug                Displays additional output
     --man                  Displays the full embedded manual
     --version              Displays the version and then exits

=head1 DESCRIPTION

Neet.pl is an enhanced port of neet.sh by onodera-punpun. It is intended
as a convenient method to manage your place in one or more video collections on
the local system.

On first run, the necessary accompanying files will be generated and your
$EDITOR launched in order to verify or fill in the needed information. A
template for the list file is provided as one-off comments and is also
duplicated below. When adding a show name, ensure that you either type the full
name of the folder or enough of it to make it unique. If there's any possible
conflict or additional matches (including multiple seasons), the script will
prompt you to tell it what you mean after closing your editor. It is not
necessary, however, to fill in the shows immediately. This can be done later
with --add-show, as documented below.

From there, calling the script with no arguments will launch the video player
(smplayer by default) with the current episode of the current show. When you're
ready to move on to the next episode, simply call the script with '+' to
increment the episode count and continue on. Alternatively, if you feel the
need to view the previous episode, simply call the script with '-'. Multiple '+'
and '-' options may be specified, though a mix of them will not work. It is
also possible to watch multiple episodes in succession by using --binge <num>;
it will queue up <num> episodes starting, as above, from the current episode
and also, as above, accepting '+' or '-' as modifiers

Note that using --binge sends a mass list of episodes to your defined media
player, so make sure it knows what to do with it. Note also that it immediately
records the final episode as the current episode in your list file, so ensure
you're committed before doing so.

Attempting to increment beyond the final episode will trigger an offer to
remove that show from the list or reset it to episode 1, after which you will
be offered to change to a new default show if you have any others defined.

Attempting to decrement lower than 1 is of course not allowed and will reset
the episode count to 1.

To change shows at any time, either edit the list file or call the script with
'--active-show' to select from the currently defined shows. When using
--active-show with no numerical argument, and when calling --list, a unique ID
number will be shown in front of each show. Supplying --active-show with this
number will skip interactive selection and set the show accordingly.

To add a show, either edit the list file (found at ~/.config/neet/list) or call
the script with --add-show showName. When adding a show, various configurables
filters will be applied, as defined in 'neet.conf'. This is primarily intended
to weed out things like bonus content or clean opening/ending sequences. If the
added show is the only show, it will automatically be set to active.

To remove a show, call the script with --remove-show with either the name of
the show or its ID number.

=head1 CONFIGURATION

The script relies primarily on two files, 'list' and 'alias', both stored in
~/.config/neet, with a third file, 'neet.conf', holding a few important but
essentially one-off settings.

The 'list' file is used to store a listing of available shows as well as the
episode count, e.g.

 Madoka
 + 1/12
 Evangelion
 1/26

Note that there are no blank lines between entries and no comments. The '+'
sign designates the currently active show. The number to the left of the slash
is the current episode, with the number to the right being total episodes.

The 'alias' file is generally created automatically and has a format as follows:

 Madoka=Mahou Shoujo Madoka Magica/
 Evangelion=Neon Genesis Evangelion/

The name to the left of the equal sign is the name of the show, taken from the
'list' file, and the name on the right is the path to the show's directory
relative to the directory specified in ~/.config/neet/neet.conf, by default
~/Videos/Anime.

The neet.conf file is generated on the first run and has a format as follows:

 filters=Bonus, Extras, Films, OP and ED, Special Features
 media_dir=/home/foo/Videos/Anime
 player=smplayer
 player_options=-actions "fullscreen"
 valid_extensions=.avi, .flv, .mkv, .mp4, .ogm, .rm

Again, no comments are allowed, though it should be otherwise fairly
self-explanatory. If your preferred player isn't in your $PATH for whatever
reason, be sure to specify the full path to its binary.

Also, ensure that the filters and the valid extensions are written correctly,
with a comma followed by a space after all but the final items.

=head1 DEPENDENCIES

Config::Tiny (dpkg libconfig-tiny-perl || cpan Config::Tiny)
Sort::Naturally (dpkg libsort-naturally-perl || cpan Sort::Naturally)
Smart::Comments (dpkg libsmart-comments-perl || cpan Smart::Comments)

A video player of some sort, smplayer being the default; this can be
changed in ~/.config/neet/neet.conf.

=head1 BUGS AND LIMITATIONS

The script may act oddly if the 'list' or 'alias' files are malformed. Try not
to do that.

Don't try and be clever by passing in + and - arguments simultaneously, e.g.
'+++-'. There's no reason to do that and such arguments will be quietly
ignored.

--binge, --add-show, --remove-show, and --choose-episode will quietly be ignored
if not given values

Report any bugs found to Surlent.

=head1 AUTHORS

 Camille (onodera-punpun) <onodera@openmailbox.com>
 Surlent <surlent777@gmail.com>

=head1 LICENSE AND COPYRIGHT

 MIT License

=cut
