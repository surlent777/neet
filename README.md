# neet

Neet.pl is an enhanced port of neet.sh by onodera-punpun. It is intended as a
convenient method to manage your place in one or more video collections on the
local system.

The original bash script was dropped and is now maintained as a fish script,
which can be found [here][neet-fish].

Both onodera's script and this one are released under the MIT license.

[neet-fish]: https://github.com/onodera-punpun/neet
